#!/usr/bin/env node

const downloadFile = require('./dlf');

require('yargs')
    .usage('$0 [-c chunksize_in_bytes] [-o outputfile] [-p num_reads_in_parallel] [-h] <URL>', '', (yargs) => {
    yargs.describe('c', 'download chunk size in bytes')
    yargs.default('c', 1048576) // a megabyte
    yargs.describe('o', 'output file name')
    yargs.default('o', 'outputFile.dat')
    yargs.describe('i', 'remote file start index')
    yargs.default('i', 0)
    yargs.describe('m', 'max chunks to receive')
    yargs.default('m', 4)
    yargs.describe('a', 'download entire file (overrides -m)')
    yargs.default('a', false)
    yargs.describe('p', 'parallel')
    yargs.default('p', 1)
    yargs.describe('d', 'debug')
    yargs.default('d', false)
    yargs.boolean(['a','d'])
    // yargs.default('d', false)
	yargs.help('h')
    yargs.alias('h', 'help')
    yargs.positional('URL',  {
        describe: 'the URL of the file to download',
        type: 'string'
        })
            }, (argv) => {
                let args = {
                    chunkSize: argv.c,
                    maxChunks: argv.m,
                    getAll: argv.a,
                    startIndex: argv.i,
                    oFile: argv.o,
                    nParallel: argv.p,
                    debug: argv.d,
                    url: argv.URL
                }
                console.info(`Downloading ${argv.URL}`)
                var dlf = downloadFile.setup(args);
                dlf.initiate( (err, res) => {
                    if (err) {
                        console.log(err)
                    }
                    else {
                        console.log(`${res.writtenKBytes}kb written in ${res.seconds} seconds as ${res.nChunks} ${res.chunkSize} chunks`);
                    }
                });
        })
  .argv