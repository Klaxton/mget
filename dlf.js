
const request = require('request');
const fs = require('fs');

module.exports.setup = function(args) {
    return new DataFetcher(args);
}

/*
    Instantiate as an object. Fetches data in the specified chunk size from
    a remote server. Multiple reads can optionally be done in parallel.
*/
function DataFetcher(args) {
    var that = this;
    try {
        that.nParallel = args.nParallel;
        that.chunkSize = args.chunkSize;
        that.maxChunks = args.maxChunks;
        that.debug = args.debug;
        that.oFile = args.oFile;
        that.getAll = args.getAll;
    }
    catch (err) {
        throw(err);
    }

    if ( (!that.getAll) && (that.nParallel > that.maxChunks) )
        that.nParallel = that.maxChunks;

    that.readQueue = [];    // pending reads from remote file
    that.writeQueue = [];   // pending writes to local file
    that.nChunks = 0;       // number of chunks requested
    that.runStatus = 'closed';
    that.inFlight = 0;
    that.nextIndex = args.startIndex;
    that.writeFp = null;
    that.startTime = 0;
    
    this.initiate = (callback) => {
        that.callback = callback;
        try {
            fs.open(that.oFile, 'w', (err, fp) => {
                if (err)
                    throw(err);
                that.writeFp = fp;
                that.runStatus = 'open';
                that.startTime = Date.now()
                startup();
            });
        }
        catch (err) {
            throw(err);
        }
    }

    function startup() {
        if (that.runStatus != 'open')
            return;
        fillReadQueue();
        readEntries();
    }

    // put reads in the queue up to parallel limit
    function fillReadQueue() {
        let qSpace = that.nParallel - that.readQueue.length;

        var nc = that.nChunks;

        if ( (!that.getAll) && (that.nChunks >= that.maxChunks) ) {
            that.runStatus = 'closing';
            return;
        }
        else if (that.runStatus == 'open') {
            for (var i = 0; i < qSpace; i++) {
                if (that.nChunks >= that.maxChunks)
                    return;
                let entry = makeEntry(that.nextIndex, that.chunkSize);
                that.readQueue.push(entry);
                that.nextIndex += that.chunkSize;
                that.nChunks++;
            }
        }
    }

    function makeEntry(index, nBytes) {
        var entry = {
            startIndex: index,
            nBytes: nBytes
        };
        return entry;
    }

    function readEntries() {
        while (that.inFlight < that.nParallel) {
            let entry = that.readQueue.pop();
            if (!entry)
                return;
            that.inFlight++;
            getChunk(args.url, entry.startIndex, entry.nBytes, chunkResult)
        }
    }

    // if we didn't get all the data we asked for queue a read for the remainder
    function checkForSkimped(result) {
        if (that.runStatus == 'closing')
            return;
        if (result.receivedSize < that.chunkSize) {
            let index = result.startIndex + result.receivedSize;
            let nBytes = that.chunkSize - result.receivedSize;
            if (that.debug) console.log(`skimped ${nBytes} bytes`);
            let entry = makeEntry(index, nBytes);
            that.readQueue.push(entry);
        }
    }

    /*
        You can't write to an offset in a file while another 
        write is in progress, so we serialize the writes with a queue.
        The server can send back more than we ask for, discard the extra.
        It can also send less, enqueue reads for missing pieces.
    */
    function chunkResult(err, result) {

        if (err) {
            let e = null;
            if (err.code == "ENOTFOUND")
                e = `chunkResult: bad URL ${err.host}, exiting`; 
            else if (err.code == "ETIMEDOUT")
                e = `chunkResult: timed out on server request, exiting`;
            that.callback(e, null);
        }
        that.inFlight--;
        if (result == null) {
            return;
        }

        if ( (result.statusCode == 416) || (result.statusCode == 200) ){
            if (that.runStatus == 'closing') {
                // we may have already seen a 416 from a queued request
                return;
            }
            else
                that.runStatus = 'closing';
        }
        else 
            checkForSkimped(result);

        that.writeQueue.push(result);
        writeChunk();
    }

    // pull received data from the queue and write to file at offset
    function writeChunk() {
        let toWrite = that.writeQueue.pop();
        if (!toWrite)
            return;

        // could have received more than we asked for but discard
        // the extra, it will come in later
        let writeSize = that.chunkSize;
        if (toWrite.receivedSize < that.chunkSize)
            writeSize = toWrite.receivedSize;

        fs.write(that.writeFp, toWrite.data, 0, writeSize, toWrite.startIndex, (err, writtenBytes, buffer) => {
            if (err) {
                that.callback('error writing file: ' + err, null);
            }

            if ( (!that.getAll) && (that.nChunks >= that.maxChunks) ) {
                that.runStatus = 'closing';
            }

            // we saw 416 or wrote maxChunks and no reads are pending
            if ( (that.runStatus == 'closing') && (that.inFlight == 0) ) {
                let seconds = ((Date.now() - that.startTime) / 1000)
                fs.close(that.writeFp, function() {
                    const stats = fs.statSync(that.oFile)
                    const writtenKBytes = Math.trunc(stats.size / 1000);
                    that.callback(null, {writtenKBytes: writtenKBytes, 
                                            nChunks: that.nChunks,
                                            seconds: seconds,
                                            chunkSize: that.chunkSize
                                        });
                });
            }
            else {
                if (that.debug) console.log(`inFlight: ${that.inFlight}, ${that.runStatus}`);
                startup();      // enqueue read requests and initiate them
                writeChunk();   // initiate more writes
            }
        })
    }

    /*
        Get a chunk of data from the remote server using the Range header.
        The server responds with a 206 if the request succeeded, or 416
        if we reached end of file. 200 means the server doesn't honor the
        range header and it sent the entire file.
    */
    function getChunk(url, startIndex, chunkSize, callback) {

        var lastByteIndex = startIndex + chunkSize;
        var rangeString = `bytes=${startIndex}-${lastByteIndex}`
        var options = {
            url: url,
            encoding: null, // causes body to be a Buffer
            headers: {
                'Range': rangeString
            },
            timeout: 60000  // if chunksize too large or network slow we could time out
        }

        request.get(options, (error, response, body) => {
            if (error) {
                // console.log(error);
                callback(error, null);
            } 
            else {
                let statusCode = response.statusCode;
                let rLength = parseInt(response.headers['content-length']);
                let pString = `status; ${statusCode}, index; ${startIndex}, received; ${rLength}`;
                if (that.debug) console.log(pString);

                var result = {
                    statusCode: response.statusCode,
                    data: body,
                    startIndex: startIndex,
                    receivedSize: rLength
                }
                callback(null, result);
            }
        });
    }
}
